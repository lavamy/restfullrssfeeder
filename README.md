# README #

### Yêu cầu ###
* PHP 5.0 trở lên

### Demo ###
* Link xem trực tiếp: http://yeualo.com/minhluu/newsfeed
* Link dùng RESTful API: http://yeualo.com/minhluu/newsfeed/api/update

### Thư viện sử dụng ###
* Parse HTML file: http://simplehtmldom.sourceforge.net/

### Công cụ hỗ trợ ###
* Filezilla
* Gedit