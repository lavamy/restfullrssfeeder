<?php

include_once('simple_html_dom.php');

class News {
	public $date; 
	public $title; 
	public $link;

	public static function update() {
		$base = 'http://www.fit.hcmus.edu.vn/vn/';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_URL, $base);
		curl_setopt($curl, CURLOPT_REFERER, $base);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$str = curl_exec($curl);
		curl_close($curl);

		$html = new simple_html_dom();
		$html->load($str);

		$main = $html->find("#dnn_ctr989_ModuleContent");

		$newsList = array();

		foreach($main[0]->find("table") as $newsField) {
			$day_month = $newsField->find('tbody tr td[class=day_month]');
			$day = preg_replace("/[^A-Za-z0-9]/", "", $day_month[0]->plaintext);
			$month = preg_replace("/[^A-Za-z0-9]/", "", $day_month[1]->plaintext);
			$year = preg_replace("/[^A-Za-z0-9]/", "", $newsField->find('tbody tr td[class=post_year]')[0]->plaintext);
			$date = date(DATE_RFC2822, mktime(0, 0, 0, $day, $month, $year));

			$link = $newsField->find('tbody tr td[class=post_title] a')[0];
			$title = preg_replace("/\t/", "", $link->plaintext);
			$destination = $base.$link->href;

			$news = new News();
			$news->date = $date;
			$news->link = $destination;
			$news->title = $title;

			$newsList[] = $news;
		}

		return $newsList;
        }

	public static function toRSS($newsList) {
		$rss = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<rss version=\"2.0\">\n<channel>";

		$pubDate = date(DATE_RFC2822);
		$rss.= "<title>HCMUS FIT</title>\n";
		$rss.= "<description>Tin tuc cap nhap tu http://www.fit.hcmus.edu.vn</description>\n";
		$rss.= "<link>http://www.fit.hcmus.edu.vn</link>\n";
		$rss.= "<lastBuildDate>".$pubDate."</lastBuildDate>\n";
		$rss.= "<pubDate>".$pubDate."</pubDate>\n";
		$rss.= "<ttl>1800</ttl>\n";
		foreach($newsList as $news) {
			$rss.= "<item>\n";
			$rss.= "<title>".$news->title."</title>\n";
			$rss.= "<description>".$news->title."</description>\n";
			$rss.= "<link>".$news->link."</link>\n";
			$rss.= "<pubDate>".$news->date."</pubDate>\n";
			$rss.= "</item>\n";
		}

		$rss.= "</channel></rss>";

		return $rss;
	}
} 
?>
