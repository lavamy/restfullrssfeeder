<?php

require_once 'API.php';
require_once '../news_feed.php';

class FeedAPI extends API
{
    public function __construct($request, $origin) {
        parent::__construct($request);
	
	//skip authentication

	/*
        $APIKey = new Models\APIKey();
        $User = new Models\User();

        if (!array_key_exists('apiKey', $this->request)) {
            throw new Exception('No API Key provided');
        } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
            throw new Exception('Invalid API Key');
        } else if (array_key_exists('token', $this->request) &&
             !$User->get('token', $this->request['token'])) {

            throw new Exception('Invalid User Token');
        }

        $this->User = $User;
	*/
    }

     protected function update() {
        if ($this->method == 'GET') {
		$list = News::update();
		return News::toRSS($list);
        } else {
            return "Only accepts GET requests";
        }
     }
 }

?>
